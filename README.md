csv2db.py
---------

The purpose of this script is to take a CSV file and populate it into a database
table for easier access programatically.  Once the data from the csv file is in
the database, you can simply use whatever db module you are comfortable with to
access the informaiton.  Otherwise, you would have to parse the csv file each 
time that you need information. 


