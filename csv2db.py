#!/usr/bin/env python

import csv

class csv2db:
    ''' A class for importing a csv file and populating it into a database '''

    def __init__(self):
        print "Class initialized"

    def csv_import(self, filepath, separator=","):
        '''
        Description: Function used to take a .csv file and process it into its parts:
          - field titles
          - lines of data
    
        Usage:  csv_import(file, separator=",")

          file:       The full path to, including the file name to be processed.
          separator:  The separator used for the  data fields.  Defaults to a comma.
        '''
        print "Separator is ${separator}"

    def add_to_db(self, headers_array, data_array):
        '''
        Description: Function to take the imported csv data and populate it into the
          database.

        Usage:  add_to_db(headers_array, data_array)

          headers_array:  The array containing the table/field headers
          data_array:     The array containing the table data
        '''

# - How to process input if called as its own script versus as a module?
# - Need to specify db, table, etc, that is needed to write to.
# - Need to get data from file into appropriate containers and then import.

if __name__ == "__main__":
    pass
