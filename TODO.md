.:: TODO ::.

- Be able to read in a csv file:
	- Read 1st line containing fields
	- Read in rest of data, line by line into a db table

- Support (initially) for:
	- MySQL
	- MongoDB

- Ensure this is written as a module, but is also usable on its own

